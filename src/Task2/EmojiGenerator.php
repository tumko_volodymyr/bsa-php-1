<?php

declare(strict_types=1);

namespace App\Task2;

class EmojiGenerator
{
    /**
     * @var array
     */
    private const EMOJI = ['🚀', '🚃', '🚄', '🚅', '🚇'];

    /**
     * @return \Generator
     */
    public function generate(): \Generator
    {
        yield from self::EMOJI;
    }
}

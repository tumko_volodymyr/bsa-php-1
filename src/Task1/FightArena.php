<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{

    /**
     * @var Fighter []
     */
    private $fighters=[];

    /**
     * @param Fighter $fighter
     */
    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    /**
     * @return Fighter|null
     */
    public function mostPowerful(): ?Fighter
    {
        return $this->reduceFightersByCondition(function (Fighter $carry, Fighter $item){
            return $carry->getAttack() < $item->getAttack();
        });
    }

    /**
     * @return Fighter|null
     */
    public function mostHealthy(): ?Fighter
    {
        return $this->reduceFightersByCondition(function (Fighter $carry, Fighter $item){
            return $carry->getHealth() < $item->getHealth();
        });
    }

    /**
     * @return Fighter[]
     */
    public function all(): array
    {
        return $this->fighters;
    }

    /**
     * @param callable $conditionForChangeCarryFunction
     * @return Fighter|null
     */
    private function reduceFightersByCondition (callable  $conditionForChangeCarryFunction ) : ?Fighter{
        return array_reduce($this->fighters, function (?Fighter $carry, Fighter $item ) use ($conditionForChangeCarryFunction) {
            if (null === $carry || $conditionForChangeCarryFunction($carry, $item)) {
                return $item;
            }
            return $carry;
        });
    }
}

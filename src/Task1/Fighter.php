<?php

declare(strict_types=1);

namespace App\Task1;

class Fighter
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $health;
    /**
     * @var int
     */
    private $attack;
    /**
     * @var string
     */
    private $image;

    /**
     * Fighter constructor.
     * @param int $id
     * @param string $name
     * @param int $health
     * @param int $attack
     * @param string $image
     */
    public function __construct( int $id, string $name, int $health, int $attack, string $image)
    {
        $this->id = $id;
        $this->name = $name;
        $this->health = $health;
        $this->attack = $attack;
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @return int
     */
    public function getAttack(): int
    {
        return $this->attack;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
       return $this->image;
    }
}

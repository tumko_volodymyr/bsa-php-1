<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $fighters = '';
        $fighterTemplate = '<div class="col-sm-4">
                                <div class="card">
                                  <div class="card-header">
                                    <p class="text-center">%s: %d, %d </p>
                                  </div>
                                  <div class="card-body text-center fighter" style="max-height: 350px;">
                                    <img src="%s">
                                  </div>
                                </div>
                            </div>';
        foreach ($arena->all() as $fighter){
            $fighters .= sprintf( $fighterTemplate, $fighter->getName(), $fighter->getHealth(), $fighter->getAttack(),$fighter->getImage() );
        }
        $containerTemplate = '<div class="container">
                                  <div class="row py-4">
                                    <div class="text-center col-12">
                                        <h1 id="header-and-footer"><span class="bd-content-title">Arena fighters</span></h1>
                                    </div>
                                  </div>
                                  <div class="row">
                                  %s
                                  </div>
                              </div>';
        return sprintf($containerTemplate,$fighters);
    }
}
